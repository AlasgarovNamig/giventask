package GivenTask.az.GivenTask.service;

import GivenTask.az.GivenTask.dto.EmployeeDto;
import GivenTask.az.GivenTask.exception.NotFoundException;
import GivenTask.az.GivenTask.model.Employee;
import GivenTask.az.GivenTask.reposiroty.EmployeeRepository;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.Spy;
import org.mockito.junit.jupiter.MockitoExtension;
import org.modelmapper.ModelMapper;
import org.springframework.web.bind.MethodArgumentNotValidException;

import javax.validation.ConstraintViolationException;
import java.math.BigDecimal;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.verify;
import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.AssertionsForClassTypes.assertThatThrownBy;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.client.ExpectedCount.times;

@ExtendWith(MockitoExtension.class)
class EmployeeServiceTest {


    private static final Long DUMMY_ID = 1l;
    private static final String NAME = "Namig";
    private static final String SURNAME = "Alasgarov";
    private static final BigDecimal SALARY = new BigDecimal(1.35);


    @Mock
    private EmployeeRepository employeeRepository;

    @Spy
    private ModelMapper modelMapper;

    @InjectMocks
    private EmployeeServiceImpl employeeService;

    private List<Employee> dummyList;
    private  Employee emp;
    private EmployeeDto empDto;


    @BeforeEach
    void setUp() {
        emp = new Employee(DUMMY_ID,NAME,SURNAME,SALARY);
        empDto = new EmployeeDto(NAME,SURNAME,SALARY);
        dummyList = Arrays.asList(emp,emp);
    }

    @Test
    void givenGetEmployeeIdExpectNotFountException() {
        assertThatThrownBy(() -> employeeService.getEmployeeById(DUMMY_ID))
                .isInstanceOf(NotFoundException.class).hasMessage("Employee not found with this id");
    }

    @Test
    void givenDeleteEmployeeIdExpectNotFountException() {
        assertThatThrownBy(() -> employeeService.deleteEmployee(DUMMY_ID))
                .isInstanceOf(NotFoundException.class).hasMessage("Employee not found with this id");
    }

    @Test
    void whenGetAllEmployeeExpectedEmployeeList() {

        when(employeeRepository.findAll()).thenReturn(dummyList);

        assertThat(employeeService.getAllEmployee()).isEqualTo(dummyList);
    }

    @Test
    void givenIdWhenDeleteByIdThenDelete() {

        when(employeeRepository.findById(any(Long.class))).thenReturn(Optional.of(emp));

        employeeService.deleteEmployee(DUMMY_ID);

        Mockito.verify(employeeRepository, Mockito.times(1)).delete(any(Employee.class));

    }

    @Test
    void givenEmployeeDtoExpectCallCreateEmployeeMethod() {

        employeeService.createEmployee(empDto);

        Mockito.verify(employeeRepository, Mockito.times(1)).save(modelMapper.map(empDto, Employee.class));

    }

    @Test
    void givenIdWhenCallFindByIdThenNullInUpdate() {

        when(employeeRepository.findById(any(Long.class))).thenReturn(null);

        employeeService.updateEmployee(1l, empDto);

        Mockito.verify(employeeRepository, Mockito.times(1)).save(modelMapper.map(empDto, Employee.class));

    }

    @Test
    void givenIdWhenCallFindByIdThenNotNullInUpdate() {

        when(employeeRepository.findById(any(Long.class))).thenReturn(Optional.of(emp));

        employeeService.updateEmployee(1l, empDto);

        Mockito.verify(employeeRepository, Mockito.times(1)).save(modelMapper.map(emp, Employee.class));

    }


}