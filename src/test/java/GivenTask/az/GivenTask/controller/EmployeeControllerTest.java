package GivenTask.az.GivenTask.controller;

import GivenTask.az.GivenTask.dto.EmployeeDto;
import GivenTask.az.GivenTask.model.Employee;
import GivenTask.az.GivenTask.service.EmployeeServiceImpl;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.hamcrest.Matchers.is;

@ExtendWith(SpringExtension.class)
@WebMvcTest(EmployeeController.class)
class EmployeeControllerTest {

    private static final Long DUMMY_ID = 1l;
    private static final String NAME = "Namig";
    private static final String SURNAME = "Alasgarov";
    private static final BigDecimal SALARY = new BigDecimal(1.35);


    @MockBean
    private EmployeeServiceImpl employeeService;

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private ObjectMapper objectMapper;


    private Employee emp;
    private EmployeeDto empDto;
    private List<Employee> dummyList;

    @BeforeEach
    void setUp() {
        emp = new Employee(DUMMY_ID,NAME,SURNAME,SALARY);
        empDto = new EmployeeDto(NAME,SURNAME,SALARY);
        dummyList = Arrays.asList(emp,emp);

    }

    @Test
    void getAllEmployee() throws Exception {


        when(employeeService.getAllEmployee()).thenReturn(dummyList);

        mockMvc.perform(get("/employees/" )
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());
    }

    @Test
    void getEmployeeById() throws Exception {

        when(employeeService.getEmployeeById(DUMMY_ID)).thenReturn(empDto);

        mockMvc.perform(get( "/employees/" + DUMMY_ID.toString())
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.name", is(NAME)))
                .andExpect(jsonPath("$.surname", is(SURNAME)))
                .andExpect(jsonPath("$.salary", is(SALARY)));

    }

    @Test
    void postEmployee() throws Exception {

        ResultActions actions = mockMvc
                .perform(post( "/employees/" )
                        .contentType("application/json")
                        .content(objectMapper.writeValueAsString(empDto))
                );


         Mockito.verify(employeeService, Mockito.times(1)).createEmployee(any(EmployeeDto.class));
         actions.andExpect(status().is2xxSuccessful());
    }
    @Test
    void updateEmployee() throws Exception {

        ResultActions actions = mockMvc
                .perform(put( "/employees/" + DUMMY_ID.toString())
                        .contentType("application/json")
                        .content(objectMapper.writeValueAsString(empDto))
                );

        Mockito.verify(employeeService, Mockito.times(1)).updateEmployee(any(Long.class),any(EmployeeDto.class));
        actions.andExpect(status().isOk());
    }

    @Test
    void deleteEmployee() throws Exception {

        ResultActions actions = mockMvc
                .perform(delete( "/employees/" + DUMMY_ID.toString())
                        .contentType("application/json")
                );

        Mockito.verify(employeeService, Mockito.times(1)).deleteEmployee(any(Long.class));
        actions.andExpect(status().is2xxSuccessful());
    }

}