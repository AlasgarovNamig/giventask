package GivenTask.az.GivenTask;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class GivenTaskApplication {

	public static void main(String[] args) {
		SpringApplication.run(GivenTaskApplication.class, args);
	}

}
