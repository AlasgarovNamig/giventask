package GivenTask.az.GivenTask.model;

import liquibase.pro.packaged.C;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.NonNull;

import javax.persistence.*;
import javax.validation.constraints.*;
import java.math.BigDecimal;

@Data
@Entity
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "employee")
public class Employee {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Min(0)
    @Positive(message = "  Employee id can not be negative")
    private Long id;

    @Column(name = "name")
    @Size(min = 3,max = 50)
    @NotBlank(message = "Name can not be neither null nor empty")
    private String name;

    @Column(name = "surname")
    @Size(min = 3,max = 50)
    @NotBlank(message = "Name can not be neither null nor empty")
    private String surname;

    @Column(name = "salary")
    @NotNull(message = "Salary can not be neither null nor empty")
    @Positive(message = "salary can not be negative")
    private BigDecimal salary;

}
