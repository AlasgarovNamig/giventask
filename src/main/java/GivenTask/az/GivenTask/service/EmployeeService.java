package GivenTask.az.GivenTask.service;

import GivenTask.az.GivenTask.dto.EmployeeDto;
import GivenTask.az.GivenTask.model.Employee;

import java.util.List;

public interface EmployeeService {
    List<Employee> getAllEmployee();
    EmployeeDto getEmployeeById(Long id);
    void createEmployee(EmployeeDto employeeDto);
    void updateEmployee(Long id, EmployeeDto employeeDto);
    void deleteEmployee(Long id);

}
