package GivenTask.az.GivenTask.service;

import GivenTask.az.GivenTask.dto.EmployeeDto;
import GivenTask.az.GivenTask.model.Employee;
import GivenTask.az.GivenTask.reposiroty.EmployeeRepository;
import javassist.NotFoundException;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.MethodArgumentNotValidException;


import java.util.List;

@Service
@RequiredArgsConstructor
public class EmployeeServiceImpl implements EmployeeService {
    private final ModelMapper mapper;
    private final EmployeeRepository employeeRepository;

    @Override
    public List<Employee> getAllEmployee() {
        return employeeRepository.findAll();
    }

    @Override
    public EmployeeDto getEmployeeById(Long id) {
        return employeeRepository
                .findById(id)
                .map(employee -> mapper.map(employee, EmployeeDto.class))
                .orElseThrow(GivenTask.az.GivenTask.exception.NotFoundException::new);
    }

    @Override
    public void createEmployee(EmployeeDto employeeDto) {
        employeeRepository.save(mapper.map(employeeDto, Employee.class));
    }

    @Override
    public void updateEmployee(Long id, EmployeeDto employeeDto) {
        if (employeeRepository.findById(id) != null) {
            Employee employee = new Employee();
            mapper.map(employeeDto, employee);
            employee.setId(id);
            employeeRepository.save(employee);
        } else {
            employeeRepository.save(mapper.map(employeeDto, Employee.class));
        }
    }

    @Override
    public void deleteEmployee(Long id) {
        Employee emp = employeeRepository.findById(id).orElseThrow(GivenTask.az.GivenTask.exception.NotFoundException::new);
        employeeRepository.delete(emp);
    }
}
