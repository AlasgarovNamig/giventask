package GivenTask.az.GivenTask.exception;

public class NotFoundException extends RuntimeException {
    private static final long serialVersionUID = 58432132465811L;
private static final String msg = "Employee not found with this id";

    public NotFoundException() {
        super(msg);
    }
}
